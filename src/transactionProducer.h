#ifndef PHP_TRANSACTIONPRODUCER_H
#define PHP_TRANSACTIONPRODUCER_H

#include <iostream>
#include <phpcpp.h>
#include <rocketmq/TransactionMQProducer.h>
#include <rocketmq/Message.h>
#include <rocketmq/SendResult.h>
#include <rocketmq/MQClientException.h>

class TransactionProducer : public Php::Base {
public:
	void __construct(Php::Parameters &params);
	void __destruct();
    void start();
    void setProducerGroup(Php::Parameters &params);
    void endTransaction(Php::Parameters &params);
    Php::Value sendMessageInTransaction(Php::Parameters &params);

    TransactionProducer() {
		transactionProducer = new TransactionMQProducer;
    }
    virtual ~TransactionProducer() {
    	delete(transactionProducer);
    }

private:
    TransactionMQProducer* transactionProducer;
    TransactionSendResult sendResult;
};

#endif