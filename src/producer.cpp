#include <producer.h>
#include <phpSendResult.h>

void Producer::__construct(Php::Parameters &params)
{
}

void Producer::__destruct()
{
    producer->shutdown();
}

void Producer::start()
{
    producer->start();
}

void Producer::setProducerGroup(Php::Parameters &params)
{
    if (params.size() != 1) {
        throw Php::Exception("Incorrect number of parameters assigned.");
    }

    producer->setProducerGroup(params[0].stringValue());
}

Php::Value Producer::send(Php::Parameters &params) {
    if (params.size() != 4) {
        throw Php::Exception("Incorrect number of parameters assigned.");
    }

	std::string value(params[3].stringValue());

	Message msg(params[0].stringValue(),// topic
                        params[1].stringValue(),// tag
                        params[2].stringValue(),// key
                        value.c_str(),// body
                        value.size());

    SendResult sendResult;
    try {
        sendResult = producer->send(msg);
    }
	catch(MQClientException e) {
        throw Php::Exception(e.what());
    }

    TransactionSendResult transactionSendResult;
    transactionSendResult.setSendStatus(sendResult.getSendStatus());
    transactionSendResult.setMessageQueue(sendResult.getMessageQueue());
    transactionSendResult.setMsgId(sendResult.getMsgId());
    transactionSendResult.setQueueOffset(sendResult.getQueueOffset());
    transactionSendResult.setTransactionId(sendResult.getTransactionId());
    transactionSendResult.setUniqKey(msg.getProperty(Message::PROPERTY_UNIQ_CLIENT_MESSAGE_ID_KEYIDX));

    PhpSendResult *phpSendResult = new PhpSendResult(transactionSendResult);
	Php::Value result = Php::Object("PhpSendResult", phpSendResult);

    return result;
}
