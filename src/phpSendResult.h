#ifndef PHP_SENDRESULT_H
#define PHP_SENDRESULT_H

#include <iostream>
#include <phpcpp.h>
#include <rocketmq/SendResult.h>

class PhpSendResult : public Php::Base {
public:
    PhpSendResult(TransactionSendResult sendResult) {
        pSendResult = sendResult;
    }

    virtual ~PhpSendResult() {
    }

    Php::Value getMsgId();
    Php::Value getUniqKey();

private:
    TransactionSendResult pSendResult;
};

#endif