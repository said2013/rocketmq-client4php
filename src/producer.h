#ifndef PHP_PRODUCER_H
#define PHP_PRODUCER_H

#include <iostream>
#include <phpcpp.h>
#include <rocketmq/DefaultMQProducer.h>
#include <rocketmq/Message.h>
#include <rocketmq/SendResult.h>
#include <rocketmq/MQClientException.h>

class Producer : public Php::Base {
public:
	void __construct(Php::Parameters &params);
	void __destruct();
    void start();
    void setProducerGroup(Php::Parameters &params);
    Php::Value send(Php::Parameters &params);

    Producer() {
		producer = new DefaultMQProducer;
    }
    virtual ~Producer() {
    	delete(producer);
    }

private:
    DefaultMQProducer* producer;
};

#endif